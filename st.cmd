require essioc
require xtpico,0.12.0+0

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX",               "{{P}}{{R}}")
epicsEnvSet("DEVICE_IP",            "{{IP}}")

epicsEnvSet("I2C_COMM_PORT",        "AK_I2C_COMM")
epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")

iocshLoad("$(xtpico_DIR)/pin-diode.iocsh")

iocInit()
